import 'package:flutter/material.dart';
import 'home_view.dart';

class OptionsPage extends StatelessWidget {
  const OptionsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context, MaterialPageRoute(builder: (context) {
                return const HomePage();
              }));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text("Dzykr Settings"),
        backgroundColor: Colors.green,
      ),
      body: const Placeholder(),
    );
  }
}
